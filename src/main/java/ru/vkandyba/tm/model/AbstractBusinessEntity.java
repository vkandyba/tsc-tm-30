package ru.vkandyba.tm.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class AbstractBusinessEntity extends AbstractEntity {

    protected String userId = null;

}
