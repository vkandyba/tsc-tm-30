package ru.vkandyba.tm.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
public abstract class AbstractEntity implements Serializable {

    protected String id = UUID.randomUUID().toString();

}
