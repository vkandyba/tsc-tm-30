package ru.vkandyba.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.vkandyba.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class DataJaxBLoadXMLCommand extends AbstractDataCommand{

    @Override
    public String name() {
        return "data-jaxb-load-xml";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Data JaxB load XML";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final File file = new File(FILE_XML_JAXB);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }
}
