package ru.vkandyba.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.exception.entity.ProjectNotFoundException;
import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectFinishByIdCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "project-finish-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Finish project by id...";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter id");
        @Nullable String id = TerminalUtil.nextLine();
        @NotNull Project project = serviceLocator.getProjectService().finishById(userId, id);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
